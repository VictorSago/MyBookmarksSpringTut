### Tutorial

## Building a REST service with SpringBoot

Following the Spring REST [tutorial](https://spring.io/guides/tutorials/bookmarks/). 

The complete code for the whole tutorial is on GitHub at [https://github.com/joshlong/bookmarks/tree/tutorial](https://github.com/joshlong/bookmarks/tree/tutorial)

A newer version of the code can be found here: [https://github.com/spring-guides/tut-bookmarks](https://github.com/spring-guides/tut-bookmarks)
