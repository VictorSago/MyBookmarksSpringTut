package vic.springtuts.bookmarks;

import java.net.URI;
import java.util.stream.Collectors;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class BookmarkHRController {

    private final BookmarkRepository bookmarkRepository;
    private final AccountRepository accountRepository;

    public BookmarkHRController(BookmarkRepository bookmarkRepository, AccountRepository accountRepository) {
        this.bookmarkRepository = bookmarkRepository;
        this.accountRepository = accountRepository;
    }

    @GetMapping
    ResourceSupport root() {
        ResourceSupport root = new ResourceSupport();
        root.add(accountRepository.findAll().stream()
                .map(account -> linkTo(methodOn(BookmarkHRController.class)
                        .readBookmarks(account.getUsername()))
                        .withRel(account.getUsername()))
                .collect(Collectors.toList()));
        return root;
    }

    @GetMapping("/{userId}/bookmarks")
    Resources<BookmarkResource> readBookmarks(@PathVariable String userId) {
        this.validateUser(userId);
        return new Resources<>(bookmarkRepository.findByAccountUsername(userId).stream()
                .map(BookmarkResource::new)
                .collect(Collectors.toList()));
    }

    /**
     * Find a single bookmark and transform it into a {@link BookmarkResource}.
     *
     * @param userId
     * @param bookmarkId
     * @return
     */
    @GetMapping("/{userId}/bookmarks/{bookmarkId}")
    BookmarkResource readBookmark(@PathVariable String userId, @PathVariable Long bookmarkId) {
        this.validateUser(userId);

        return this.bookmarkRepository.findById(bookmarkId)
                .map(BookmarkResource::new)
                .orElseThrow(() -> new BookmarkNotFoundException(bookmarkId));
    }

    @PostMapping("/{userId}/bookmarks")
    ResponseEntity<?> add(@PathVariable String userId, @RequestBody Bookmark input) {
        this.validateUser(userId);
        return accountRepository.findByUsername(userId)
                .map(account -> ResponseEntity
                        .created(URI.create(
                                new BookmarkResource(bookmarkRepository.save(Bookmark.from(account, input)))
                                        .getLink("self").getHref()))
                        .build())
                .orElse(ResponseEntity.noContent().build());
    }

    /**
     * Verify the {@literal userId} exists.
     *
     * @param userId The Id of user whose existence is to be validated
     */
    private void validateUser(String userId) {
        this.accountRepository.findByUsername(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
    }
}
