package vic.springtuts.bookmarks;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class HateoasApplication {

    public static void main(String[] args) {
        System.out.println("Running HATEOAS Application...");
        SpringApplication.run(HateoasApplication.class, args);
    }

    @Bean
    CommandLineRunner init(AccountRepository accountRepository, BookmarkRepository bookmarkRepository) {
        return (args) ->
                Arrays.asList("jhoeller,dsyer,pwebb,ogierke,rwinch,mfisher,mpollack,jlong".split(","))
                        .forEach(uname -> {
                            Account account = accountRepository.save(new Account(uname, "ASecretPassword"));
                            bookmarkRepository.save(new Bookmark(account, "http://bookmark.com/1/" + uname, "Some description."));
                            bookmarkRepository.save(new Bookmark(account, "http://bookmark.com/2/" + uname, "Another description."));
                        });
    }
}
