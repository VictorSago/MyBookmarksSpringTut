package vic.springtuts.bookmarks;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class BookmarkResource extends ResourceSupport {

    private final Bookmark bookmark;

    public BookmarkResource(Bookmark bookmark) {
        this.bookmark = bookmark;
        String userName = bookmark.getAccount().getUsername();

        this.add(new Link(bookmark.getUri(), "bookmark-uri"));
        this.add(linkTo(BookmarkHRController.class, userName).withRel("bookmarks"));
        this.add(linkTo(methodOn(BookmarkHRController.class, userName).readBookmark(userName, bookmark.getId()))
                .withSelfRel());
    }

    public Bookmark getBookmark() {
        return bookmark;
    }
}
